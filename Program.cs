﻿bool[] doorState = new bool[100];
Array.Fill(doorState, false);

for (int state = 1; state <= doorState.Length; state++)
{
    for (int door = state - 1; door < doorState.Length; door += state)
    {
        doorState[door] = !doorState[door];
    }
}

for (int display = 0; display < 10; display++)
{
    for (int print = 0; print < 10; print++)
    {
        char finalDoorState = doorState[display * 10 + print] ? '@' : '#';
        Console.Write(finalDoorState + " ");
    }
    Console.WriteLine();
}

